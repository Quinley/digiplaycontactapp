// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'

var digiPlayContacts = angular.module('starter', ['ionic','firebase'])

.run(function($ionicPlatform, $ionicPopup){
  $ionicPlatform.ready(function(){
    //Checks internet connection
    if(window.Connection){
      if(navigator.connection.type == Connection.NONE){
        var alertPopup = $ionicPopup.alert({
          title: "Offline Mode",
          template:"Your data will be uploaded when next you establish an internet connection.",
          okText: "Ok"
        });      
      }
    }
  });
})
digiPlayContacts.factory ('Contacts', ['$firebaseArray', function($firebaseArray){
    var contactRef = new Firebase('https://qjcontactz.firebaseio.com/');
    return $firebaseArray(contactRef);
}])

digiPlayContacts.controller('NewContactCtrl', function($scope, $ionicListDelegate,$ionicPopup, Contacts){
    $scope.cust = Contacts; //Attaches the Firebase db to these scope variables
    $scope.inputs = [];
    var inputVals = [];

    $scope.saveContact = function(){

      var confirmPopup = $ionicPopup.confirm({
            /*title: "Confirmation",*/
            template:"Are you sure you want to save this contact?",
          });
      confirmPopup.then(function(res){
        if(!res){
          return;
        }else{
            var ref = new Firebase('https://qjcontactz.firebaseio.com/Numbers');
            if($scope.cust.fName && $scope.cust.lName && $scope.cust.contact){
              if ($scope.inputs.length > 0){
                
                var numIndex = 0;
                for (i in $scope.inputs){
                  var valuez = document.getElementById("newInputs"+i).value;
                  inputVals.push(valuez);//({number:valuez});
                }
                for (j in inputVals){
                  var thisThing = inputVals[j];//.number;
                  //alert(thisThing);
                }
              }
              var contactRef = ref.child($scope.cust.contact);  
              contactRef.set({
                'firstName':$scope.cust.fName,
                'lastName':$scope.cust.lName,
                'Contact':$scope.cust.contact,
                'OtherNum':inputVals,
              });
              $scope.cust.fName  ="";
              $scope.cust.lName  ="";
              $scope.cust.contact="";
              $scope.inputs.splice($scope.inputs);
              inputVals.splice(0); //Empty the array
            }else{
              var alertPopup = $ionicPopup.alert({
                title: "Missing Information",
                template:"Please enter all information before saving.",
                okText: "Ok"
              });
            }  
        }
      })
      
    };
    $scope.addContact = function(){
      if ($scope.cust.contact!=null && $scope.cust.contact!=""){
        var items = $scope.inputs.push({});
        
      }else{
        var alertPopup = $ionicPopup.alert({
          title: "No Primary Contact",
          template:"Please enter a primary contact number before adding other contacts.",
          okText: "Ok"
        });
      }
    };
    $scope.deleteInput = function(contact){
      $scope.inputs.splice($scope.inputs.indexOf(contact),1);
    }

    $scope.clearAll= function(){
      $scope.cust.fName  ="";
      $scope.cust.lName  ="";
      $scope.cust.contact="";
      $scope.inputs.splice($scope.inputs);
      inputVals.splice(0); //Empty the array
    }
   
})


